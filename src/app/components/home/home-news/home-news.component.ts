import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { DividerModule } from 'primeng/divider';

@Component({
  selector: 'app-home-news',
  standalone: true,
  imports: [CommonModule, CardModule, ButtonModule, DividerModule],
  templateUrl: './home-news.component.html',
  styleUrls: ['./home-news.component.scss'],
})
export class HomeNewsComponent {
  news = [
    {
      time: '2023-07-05 11:11',
      title: '您有22張簽核表單待簽核。',
      link: '123',
    },
    {
      time: '2023-07-03 09:32',
      title: '您有14張簽核表單待簽核。',
      link: '123',
    },
    {
      time: '2023-06-30 14:30',
      title: '您有6張簽核表單待簽核。',
      link: '123',
    },
    {
      time: '2023-06-21 10:02',
      title: '您有衛材申請待簽核。',
      link: '',
    },
  ];
}
