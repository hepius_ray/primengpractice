export interface HomeFavorite {
  id: number;
  title: string;
  version: string;
  icon: string;
  avatarBgcColor: object;
}
