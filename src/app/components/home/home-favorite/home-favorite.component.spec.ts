import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeFavoriteComponent } from './home-favorite.component';

describe('HomeFavoriteComponent', () => {
  let component: HomeFavoriteComponent;
  let fixture: ComponentFixture<HomeFavoriteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HomeFavoriteComponent]
    });
    fixture = TestBed.createComponent(HomeFavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
