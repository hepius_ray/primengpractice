import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { AvatarModule } from 'primeng/avatar';
import { ImageModule } from 'primeng/image';
import { HomeFavorite } from './home-favorite.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-favorite',
  standalone: true,
  imports: [CommonModule, CardModule, ButtonModule, AvatarModule, ImageModule],
  templateUrl: './home-favorite.component.html',
  styleUrls: ['./home-favorite.component.scss'],
})
export class HomeFavoriteComponent {
  router = inject(Router);

  favorite: HomeFavorite[] = [
    {
      id: 1,
      title: '門診醫囑系統',
      version: '1.1.0',
      icon: 'assets/img/clinic.svg',
      avatarBgcColor: { 'background-color': '#006C4F' },
    },
    {
      id: 2,
      title: '心臟移植表單',
      version: '1.1.0',
      icon: 'assets/img/heart.svg',
      avatarBgcColor: { 'background-color': '#F97316' },
    },
    {
      id: 3,
      title: '電子病歷查詢系統',
      version: '1.1.0',
      icon: 'assets/img/emr.svg',
      avatarBgcColor: { 'background-color': '#06B6D4' },
    },
    {
      id: 4,
      title: '住院醫囑系統',
      version: '1.1.0',
      icon: 'assets/img/hospital.svg',
      avatarBgcColor: { 'background-color': '#EC4899' },
    },
  ];

  onFavoriteClick(favorite: HomeFavorite) {
    switch (favorite.id) {
      case 1:
        this.router.navigate(['/clinic']);
        break;
      case 2:
        break;
      case 3:
        break;
      case 4:
        break;
    }
  }
}
