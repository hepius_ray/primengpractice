import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';

@Component({
  selector: 'app-clinic-cpoe-header',
  standalone: true,
  imports: [CommonModule, ButtonModule, InputTextModule],
  templateUrl: './clinic-cpoe-header.component.html',
  styleUrls: ['./clinic-cpoe-header.component.scss'],
})
export class ClinicCpoeHeaderComponent {}
