import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicCpoeHeaderComponent } from './clinic-cpoe-header.component';

describe('ClinicCpoeHeaderComponent', () => {
  let component: ClinicCpoeHeaderComponent;
  let fixture: ComponentFixture<ClinicCpoeHeaderComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ClinicCpoeHeaderComponent]
    });
    fixture = TestBed.createComponent(ClinicCpoeHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
