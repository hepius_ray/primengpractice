import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClinicCopeSubToolComponent } from './clinic-cope-sub-tool/clinic-cope-sub-tool.component';
import { ClinicCopeSubMainComponent } from './clinic-cope-sub-main/clinic-cope-sub-main.component';

@Component({
  selector: 'app-clinic-cope-content',
  standalone: true,
  imports: [
    CommonModule,
    ClinicCopeSubToolComponent,
    ClinicCopeSubMainComponent,
  ],
  templateUrl: './clinic-cope-content.component.html',
  styleUrls: ['./clinic-cope-content.component.scss'],
})
export class ClinicCopeContentComponent {}
