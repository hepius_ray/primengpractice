import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicCopeContentComponent } from './clinic-cope-content.component';

describe('ClinicCopeContentComponent', () => {
  let component: ClinicCopeContentComponent;
  let fixture: ComponentFixture<ClinicCopeContentComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ClinicCopeContentComponent]
    });
    fixture = TestBed.createComponent(ClinicCopeContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
