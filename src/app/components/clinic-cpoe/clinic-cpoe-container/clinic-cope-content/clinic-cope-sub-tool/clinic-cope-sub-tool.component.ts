import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'app-clinic-cope-sub-tool',
  standalone: true,
  imports: [CommonModule, ButtonModule],
  templateUrl: './clinic-cope-sub-tool.component.html',
  styleUrls: ['./clinic-cope-sub-tool.component.scss'],
})
export class ClinicCopeSubToolComponent {}
