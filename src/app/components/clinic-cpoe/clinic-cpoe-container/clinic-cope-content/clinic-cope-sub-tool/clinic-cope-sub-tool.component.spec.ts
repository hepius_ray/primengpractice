import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicCopeSubToolComponent } from './clinic-cope-sub-tool.component';

describe('ClinicCopeContentToolComponent', () => {
  let component: ClinicCopeSubToolComponent;
  let fixture: ComponentFixture<ClinicCopeSubToolComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ClinicCopeSubToolComponent],
    });
    fixture = TestBed.createComponent(ClinicCopeSubToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
