import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicCopeSubMainComponent } from './clinic-cope-sub-main.component';

describe('ClinicCopeContentMainComponent', () => {
  let component: ClinicCopeSubMainComponent;
  let fixture: ComponentFixture<ClinicCopeSubMainComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ClinicCopeSubMainComponent],
    });
    fixture = TestBed.createComponent(ClinicCopeSubMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
