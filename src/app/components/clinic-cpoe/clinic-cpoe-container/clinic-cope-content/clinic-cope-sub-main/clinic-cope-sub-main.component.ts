import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClinicCardComponent } from './clinic-card/clinic-card.component';

@Component({
  selector: 'app-clinic-cope-sub-main',
  standalone: true,
  imports: [CommonModule, ClinicCardComponent],
  templateUrl: './clinic-cope-sub-main.component.html',
  styleUrls: ['./clinic-cope-sub-main.component.scss'],
})
export class ClinicCopeSubMainComponent {}
