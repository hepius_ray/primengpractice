import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from 'primeng/card';
import { EditorModule } from 'primeng/editor';
import { TableModule } from 'primeng/table';

export enum CardContentType {
  Content,
  CheckTable,
}

export interface ClinicTableItem {
  id: number;
  state: boolean;
  class: string;
  code: string;
}

export interface ClinicCardItem {
  title: string;
  date: string;
  type: number;
  content: string | ClinicTableItem[];
}

@Component({
  selector: 'app-clinic-card',
  standalone: true,
  imports: [CommonModule, CardModule, EditorModule, TableModule],
  templateUrl: './clinic-card.component.html',
  styleUrls: ['./clinic-card.component.scss'],
})
export class ClinicCardComponent {
  CardContentType = CardContentType;
  clinicTableItems: ClinicTableItem[] = [
    {
      id: 1,
      state: false,
      class: '抗1',
      code: 'Teicoplanin',
    },
    {
      id: 1,
      state: false,
      class: '抗2',
      code: 'Teicoplanin',
    },
    {
      id: 3,
      state: false,
      class: '抗3',
      code: 'Teicoplanin',
    },
    {
      id: 4,
      state: false,
      class: '抗4',
      code: 'Teicoplanin',
    },
    {
      id: 5,
      state: false,
      class: '抗5',
      code: 'Teicoplanin',
    },
  ];

  clinicCardItems = [
    {
      title: '主訴(S)',
      date: '(2023/04/11)',
      type: CardContentType.Content,
      content:
        'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum',
    },
    {
      title: '理學檢查(O)',
      date: '(2023/04/11)',
      type: CardContentType.Content,
      content:
        'Vestibulum tempus imperdiet sem ac porttitor. Vivamus pulvinar commodo orci, suscipit porttitor velit elementum non. Fusce nec pellentesque erat, id lobortis nunc. Donec dui leo, ultrices quis turpis nec, sollicitudin sodales tortor. Aenean dapibus',
    },
    {
      title: '診斷(A)',
      date: '',
      type: CardContentType.CheckTable,
      content: this.clinicTableItems,
    },
    {
      title: '治療計畫(P)',
      date: '(2023/04/11)',
      type: CardContentType.Content,
      content:
        'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum, vitae imperdiet diam bibendum. Maecenas scelerisque orci a dolor vestibulum sagittis. Etiam quis finibus arcu, vel efficitur diam. Curabitur felis eros,',
    },
  ];
}
