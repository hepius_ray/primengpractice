import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicCardComponent } from './clinic-card.component';

describe('ClinicCardComponent', () => {
  let component: ClinicCardComponent;
  let fixture: ComponentFixture<ClinicCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ClinicCardComponent]
    });
    fixture = TestBed.createComponent(ClinicCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
