import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicCpoeContainerComponent } from './clinic-cpoe-container.component';

describe('ClinicCpoeContainerComponent', () => {
  let component: ClinicCpoeContainerComponent;
  let fixture: ComponentFixture<ClinicCpoeContainerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ClinicCpoeContainerComponent]
    });
    fixture = TestBed.createComponent(ClinicCpoeContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
