import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClinicCopeToolComponent } from './clinic-cope-tool/clinic-cope-tool.component';
import { ClinicCopeContentComponent } from './clinic-cope-content/clinic-cope-content.component';

@Component({
  selector: 'app-clinic-cpoe-container',
  standalone: true,
  imports: [CommonModule, ClinicCopeToolComponent, ClinicCopeContentComponent],
  templateUrl: './clinic-cpoe-container.component.html',
  styleUrls: ['./clinic-cpoe-container.component.scss'],
})
export class ClinicCpoeContainerComponent {}
