import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicCopeToolComponent } from './clinic-cope-tool.component';

describe('ClinicCopeToolComponent', () => {
  let component: ClinicCopeToolComponent;
  let fixture: ComponentFixture<ClinicCopeToolComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ClinicCopeToolComponent]
    });
    fixture = TestBed.createComponent(ClinicCopeToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
