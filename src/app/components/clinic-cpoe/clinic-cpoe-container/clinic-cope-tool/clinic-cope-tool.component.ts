import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { SplitButtonModule } from 'primeng/splitbutton';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'app-clinic-cope-tool',
  standalone: true,
  imports: [CommonModule, BreadcrumbModule, SplitButtonModule, ButtonModule],
  templateUrl: './clinic-cope-tool.component.html',
  styleUrls: ['./clinic-cope-tool.component.scss'],
})
export class ClinicCopeToolComponent {
  clinicBreadcrumbList = [{ label: '門診醫囑系統' }, { label: '診斷相關' }];

  items = [
    { label: '醫師上次' },
    { label: '科別上次' },
    { label: '詳細選擇' },
    { separator: true },
    { label: '預設設定' },
  ];
}
