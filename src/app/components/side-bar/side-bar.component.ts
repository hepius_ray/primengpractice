import { Component, signal, computed, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { AvatarModule } from 'primeng/avatar';
import { ImageModule } from 'primeng/image';
import { sideBarAnimations } from './side-bar-animation/side-bar-animation';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-side-bar',
  standalone: true,
  imports: [CommonModule, ButtonModule, AvatarModule, ImageModule, RouterLink],
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss'],
  animations: [sideBarAnimations],
})
export class SideBarComponent {
  sideBarToggle = signal(false);

  sideBarToggleIcon = computed(() => {
    return this.sideBarToggle()
      ? 'pi pi-angle-double-left'
      : 'pi pi-angle-double-right';
  });

  onSideBarToggle() {
    this.sideBarToggle.set(!this.sideBarToggle());
  }
}
