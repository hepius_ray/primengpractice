import {
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes,
} from '@angular/animations';

export const sideBarAnimations = [
  trigger('toggleBtn', [
    state(
      'open',
      style({
        width: '15rem',
      })
    ),
    state(
      'closed',
      style({
        width: '3.75rem',
      })
    ),
    transition('open => closed', [animate('.3s')]),
    transition('closed => open', [animate('.3s')]),
  ]),
  trigger('logoutBtn', [
    state(
      'open',
      style({
        transform: 'translateX(170%)',
      })
    ),
    state(
      'closed',
      style({
        transform: 'translateX(0%)',
      })
    ),
    transition('open => closed', [
      animate(
        '1s',
        keyframes([
          style({ transform: 'translateX(170%)', offset: 0 }),
          style({ transform: 'translateX(85%)', offset: 0.5 }),
          style({ transform: 'translateX(0%)', offset: 1 }),
        ])
      ),
    ]),
    transition('closed => open', [
      animate(
        '1s',
        keyframes([
          style({ transform: 'translateX(0%)', offset: 0 }),
          style({ transform: 'translateX(85%)', offset: 0.5 }),
          style({ transform: 'translateX(170%)', offset: 1 }),
        ])
      ),
    ]),
  ]),
  trigger('applicationBtn', [
    state(
      'open',
      style({
        transform: 'translate(85%, 100%)',
        // bottom: '-3.35rem',
      })
    ),
    state(
      'closed',
      style({
        transform: 'translate(0%)',
        bottom: '0rem',
      })
    ),
    transition('open => closed', [animate('.3s')]),
    transition('closed => open', [animate('.3s')]),
  ]),
  trigger('bellBtn', [
    state(
      'open',
      style({
        transform: 'translateY(200%)',
      })
    ),
    state(
      'closed',
      style({
        transform: 'translateY(0%)',
      })
    ),
    transition('open => closed', [animate('.3s')]),
    transition('closed => open', [animate('.3s')]),
  ]),
  trigger('avatarBtn', [
    state(
      'open',
      style({
        transform: 'translate(-95%, 310%)',
      })
    ),
    state(
      'closed',
      style({
        transform: 'translate(0%)',
      })
    ),
    transition('open => closed', [animate('.3s')]),
    transition('closed => open', [animate('.3s')]),
  ]),
  trigger('homeBtn', [
    state(
      'open',
      style({
        transform: 'translate(-170%, 395%)',
      })
    ),
    state(
      'closed',
      style({
        transform: 'translate(0%)',
      })
    ),
    transition('open => closed', [animate('.3s')]),
    transition('closed => open', [animate('.3s')]),
  ]),
  trigger('divider', [
    state(
      'open',
      style({
        width: '15rem',
        bottom: '-13rem',
      })
    ),
    state(
      'closed',
      style({
        width: '3rem',
        bottom: '0rem',
      })
    ),
    transition('open => closed', [animate('.3s')]),
    transition('closed => open', [animate('.3s')]),
  ]),
  trigger('companyLogo', [
    state(
      'open',
      style({
        transform: 'translate(-60%, 400%)',
      })
    ),
    state(
      'closed',
      style({
        transform: 'translate(0%)',
      })
    ),
    transition('open => closed', [animate('.3s')]),
    transition('closed => open', [animate('.3s')]),
  ]),
  trigger('companyName', [
    transition(':enter', [
      style({ opacity: 0 }),
      animate('1s ease-in', style({ opacity: 1 })),
    ]),
    transition(':leave', [animate('1s ease-in', style({ opacity: 0 }))]),
  ]),
];
