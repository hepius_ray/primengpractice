import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeHeaderComponent } from 'src/app/components/home/home-header/home-header.component';
import { HomeNewsComponent } from 'src/app/components/home/home-news/home-news.component';
import { HomeFavoriteComponent } from 'src/app/components/home/home-favorite/home-favorite.component';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    CommonModule,
    HomeHeaderComponent,
    HomeNewsComponent,
    HomeFavoriteComponent,
  ],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {}
