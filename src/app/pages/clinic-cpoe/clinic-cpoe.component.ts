import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { ClinicCpoeHeaderComponent } from 'src/app/components/clinic-cpoe/clinic-cpoe-header/clinic-cpoe-header.component';
import { ClinicCpoeContainerComponent } from 'src/app/components/clinic-cpoe/clinic-cpoe-container/clinic-cpoe-container.component';

@Component({
  selector: 'app-clinic-cpoe',
  standalone: true,
  imports: [
    CommonModule,
    ButtonModule,
    InputTextModule,
    ClinicCpoeHeaderComponent,
    ClinicCpoeContainerComponent,
  ],
  templateUrl: './clinic-cpoe.component.html',
  styleUrls: ['./clinic-cpoe.component.scss'],
})
export class ClinicCpoeComponent {}
