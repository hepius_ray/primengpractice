import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicCpoeComponent } from './clinic-cpoe.component';

describe('ClinicCPOEComponent', () => {
  let component: ClinicCpoeComponent;
  let fixture: ComponentFixture<ClinicCpoeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ClinicCpoeComponent],
    });
    fixture = TestBed.createComponent(ClinicCpoeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
