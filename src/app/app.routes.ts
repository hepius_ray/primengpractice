import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadComponent: () =>
      import('./layouts/main/main.component').then((x) => x.MainComponent),
    loadChildren: () =>
      import('./layouts/main/main.routes').then((x) => x.mainRoutes),
  },
  {
    path: 'login',
    loadComponent: () =>
      import('./pages/login/login.component').then((x) => x.LoginComponent),
  },
  {
    path: '**',
    redirectTo: 'login',
  },
];
