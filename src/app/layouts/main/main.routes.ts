import { Routes } from '@angular/router';

export const mainRoutes: Routes = [
  {
    path: 'home',
    loadComponent: () =>
      import('../../pages/home/home.component').then((x) => x.HomeComponent),
  },
  {
    path: 'clinic',
    loadComponent: () =>
      import('../../pages/clinic-cpoe/clinic-cpoe.component').then(
        (x) => x.ClinicCpoeComponent
      ),
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
];
